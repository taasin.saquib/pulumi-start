# set base image (host OS)
FROM python:3.7

# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .
COPY /venv/* ./venv/

# install dependencies
RUN pip install -r requirements.txt

RUN curl -fsSL https://get.pulumi.com/ | bash
RUN export PATH=$PATH:$HOME/.pulumi/bin